//
//  AppStoreReviewManager.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 15/05/2021.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import StoreKit

enum AppStoreReviewManager {
  static func requestReviewIfAppropriate() {
    SKStoreReviewController.requestReview()
  }
}
