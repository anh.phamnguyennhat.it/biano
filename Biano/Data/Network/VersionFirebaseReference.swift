//
//  VersionFirebaseReference.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 31/03/2021.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class VersionFirebaseReference {
    private var versionRef: DatabaseReference!
    
    init() {
        
    }
    
    func getVersion(completion: @escaping (Result<String, NetworkError>) -> Void) {
        guard NetworkReachability.isReachable else {
            completion(.failure(.noConnection))
            return
        }
        
        versionRef = Database.database().reference().child("version")
        versionRef.observe(.value) { (snapshot) in
            guard let value = snapshot.value as? String else {
                completion(.failure(.requestFailed))
                return
            }
            completion(.success(value))
        }
    }
}
