//
//  SongFirebaseReference.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 30/03/2021.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import CodableFirebase

enum NetworkError: Error {
    case badURL, requestFailed, unknown, noConnection
}

class SongFirebaseReference {
    var songRef: DatabaseReference!
    
    init() {
        
    }
    
    func getSongs(completion: @escaping (Result<[Song], NetworkError>) -> Void) {
        guard NetworkReachability.isReachable else {
            completion(.failure(.noConnection))
            return
        }
        songRef = Database.database().reference().child("songs")
        songRef.observe(.value) { (snapshot) in
            guard let value = snapshot.value else { return }
            do {
                let songs = try FirebaseDecoder().decode([Song].self, from: value)
                UserDefaults.songs = songs
                completion(.success(songs))
            } catch {
                completion(.failure(.requestFailed))
            }
        }
    }
    
}
