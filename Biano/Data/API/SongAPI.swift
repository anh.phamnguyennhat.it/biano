//
//  SongAPI.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 30/03/2021.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class SongAPI {
    
    private let songRef = SongFirebaseReference()
    private let versionRef = VersionFirebaseReference()
    
    func getSongs(isForce: Bool = false, completion: @escaping (Result<[Song], NetworkError>) -> Void) {
        guard !isForce else {
            // update version
            UserDefaults.latestUpdate = Date()
            versionRef.getVersion { (result) in
                if case .success(let version) = result {
                    UserDefaults.localVersion = version
                }
            }
            self.songRef.getSongs(completion: completion)
            return
        }
        
        if let latestUpdate = UserDefaults.latestUpdate, latestUpdate.isToday, let songs = UserDefaults.songs {
            completion(.success(songs))
        } else {
            versionRef.getVersion { [weak self] (result) in
                guard let self = self else { return }
                switch result {
                case .success(let version):
                    UserDefaults.latestUpdate = Date()
                    if version == UserDefaults.localVersion, let songs = UserDefaults.songs {
                        completion(.success(songs))
                    } else {
                        UserDefaults.localVersion = version
                        self.songRef.getSongs(completion: completion)
                        
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
                

            }
        }
        
        
    }
}
