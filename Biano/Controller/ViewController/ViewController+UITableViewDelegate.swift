//
//  ViewController+UITableViewDelegate.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 07/04/2021.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import Foundation
import UIKit

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredSongs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SongTableViewCell", for: indexPath) as! SongTableViewCell
        cell.set(song: filteredSongs[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        songIndex = indexPath.row
        songsContainer.isHidden = true
        playSongButton.isHidden = false
        selectSongButton.setTitle(filteredSongs[indexPath.row].name, for: .normal)
    }
    
}
