//
//  ViewController+UIGestureRecognizerDelegate.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 2/20/20.
//  Copyright © 2020 jibistudio. All rights reserved.
//

import Foundation
import UIKit
import ARKit

extension ViewController: UIGestureRecognizerDelegate {
    func registerGestureRecognizers() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
        self.sceneView.addGestureRecognizer(tapGestureRecognizer)

        let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(pinched))
        self.sceneView.addGestureRecognizer(pinchGestureRecognizer)
        pinchGestureRecognizer.delegate = self
        let rotate = UIRotationGestureRecognizer(target: self, action: #selector(handleRotation))
        rotate.delegate = self
        self.sceneView.addGestureRecognizer(rotate)
        let drag = UIPanGestureRecognizer(target: self, action: #selector(handleDrag(_:)))
        drag.delegate = self
        drag.minimumNumberOfTouches = 1
        drag.maximumNumberOfTouches = 1
        self.sceneView.addGestureRecognizer(drag)
    }
    
    @objc func tapped(recognizer :UIGestureRecognizer) {
        
        let sceneView = recognizer.view as! ARSCNView
        let touchLocation = recognizer.location(in: sceneView)
        
        let hitTestResult = sceneView.hitTest(touchLocation, types: .existingPlaneUsingExtent)
        
        if !hitTestResult.isEmpty {
            
            guard let hitResult = hitTestResult.first else {
                return
            }
            addPiano(hitResult :hitResult)
        }
    }
    
    private func addPiano(hitResult :ARHitTestResult) {
        guard !PianoNode.shared.isPlaced, let pointOfView = self.sceneView.pointOfView else {
            return
        }
        PianoNode.shared.position =  SCNVector3(hitResult.worldTransform.columns.3.x,hitResult.worldTransform.columns.3.y, hitResult.worldTransform.columns.3.z)
        let height = PianoNode.shared.distanceZ(to: pointOfView)
        PianoNode.shared.scale = SCNVector3(height*3, height*3, height*3)
        self.sceneView.debugOptions = []
        self.sceneView.scene.rootNode.addChildNode(PianoNode.shared)
        
        DispatchQueue.main.async {
            PianoNode.shared.isPlaced = true
            self.updateSessionInfoLabel(arStatus: .placed)
            self.hiddenSettingView()
            self.playSongButton.isHidden = false
            self.resetARButton.isHidden = false
        }
    }
    
    @objc func handleRotation(gesture: UIRotationGestureRecognizer) {
        var normalized: Float = (Float(PianoNode.shared.eulerAngles.y ) - Float(gesture.rotation)).truncatingRemainder(dividingBy: 2 * .pi)
        normalized = (normalized + 2 * .pi).truncatingRemainder(dividingBy: 2 * .pi)
        if normalized > .pi {
            normalized -= 2 * .pi
        }
        
        PianoNode.shared.eulerAngles.y = normalized
        gesture.rotation = 0
    }
    
    @objc fileprivate func handleDrag(_ gesture: UIRotationGestureRecognizer) {
        let point = gesture.location(in: gesture.view!)
        if let result = self.sceneView.smartHitTest(point, infinitePlane: true) {
            if let lastDragResult = lastDragResult {
                let vector: SCNVector3 = SCNVector3(result.worldTransform.columns.3.x - lastDragResult.worldTransform.columns.3.x,
                                                    result.worldTransform.columns.3.y - lastDragResult.worldTransform.columns.3.y,
                                                    result.worldTransform.columns.3.z - lastDragResult.worldTransform.columns.3.z)
                PianoNode.shared.position += vector
            }
            lastDragResult = result
        }
        
        if gesture.state == .ended {
            self.lastDragResult = nil
        }
    }
    
    @objc func pinched(gesture: UIPinchGestureRecognizer) {
        if gesture.state == UIGestureRecognizer.State.began {
            pianoStartScale = PianoNode.shared.scale.x
        }
        // apply scale and translation
        let newScale: Float = pianoStartScale * Float(gesture.scale)
        if gesture.state == UIGestureRecognizer.State.ended {
            PianoNode.shared.zoom(scale: newScale, force: true)
        } else {
            PianoNode.shared.zoom(scale: newScale)
        }
    }
}
