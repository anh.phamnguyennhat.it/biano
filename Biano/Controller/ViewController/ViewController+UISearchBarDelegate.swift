//
//  ViewController+UISearchBarDelegate.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 12/04/2021.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import Foundation
import UIKit

extension ViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard searchText != "" else {
            filteredSongs = realtimeSongs
            DispatchQueue.main.async {
                self.songTableView.reloadData()
            }
            return
        }
        
        filteredSongs = realtimeSongs.filter({ (song) -> Bool in
            let isNameContained = song.name.lowercased().contains(searchText.lowercased())
            guard let singer = song.singer else {
                return isNameContained
            }
            let isSingerContained = singer.lowercased().contains(searchText.lowercased())
            return isNameContained || isSingerContained
        })
        DispatchQueue.main.async {
            self.songTableView.reloadData()
        }
    }
}
