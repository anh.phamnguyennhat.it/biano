//
//  ViewController+PrepareData.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 04/04/2021.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import Foundation

extension ViewController {
    func prepareMidiFile(song: Song, completion: @escaping ((_ url: URL?) -> Void)) {
        if let url = song.url {
            completion(url)
        } else {
            ProgressHUBManager.shared.showIndicator(view: self.view)
            song.download { (url) in
                ProgressHUBManager.shared.hideIndicator()
                completion(url)
            }
        }
    }
    
    func prepareSongs(isForce: Bool = false) {
        songAPI.getSongs(isForce: isForce, completion: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let songs):
                DispatchQueue.main.async {
                    self.realtimeSongs = songs
                    self.filteredSongs = songs
                    self.hiddenSettingView()
                    self.selectSongButton.setTitle(self.realtimeSongs[self.songIndex].name, for: .normal)
                    self.songTableView.reloadData()
                }
                break
            case .failure(_):
                DispatchQueue.main.async {
                    self.hiddenSettingView()
                }
            }
            
        })
    }
}
