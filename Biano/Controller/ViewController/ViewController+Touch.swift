//
//  ViewController+Touch.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 04/04/2021.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import Foundation
import UIKit

extension ViewController {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard !PianoNode.shared.isPlay else { return }
        updateSessionInfoLabel(arStatus: .played)
        for touch in touches {
            let location = touch.location(in: sceneView)
            let hitTest = sceneView.hitTest(location, options: nil)
                // Assumes sprites are named "sprite"
            if let node = hitTest.first?.node.parent as? PianoKeyNode {
                selectedNodes[touch] = node
                if let noteNumber = node.noteNumber {
                    conductor.playNote(note: noteNumber, velocity: 100, channel: 0)
                }
                node.animatePressed(color: .cyan)
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard !PianoNode.shared.isPlay else { return }
        for touch in touches {
            if selectedNodes[touch] != nil {
                if let noteNumber = selectedNodes[touch]?.noteNumber {
                    conductor.stopNote(note: noteNumber, channel: 0)
                }
                selectedNodes[touch]?.animateReleased()
                selectedNodes[touch] = nil
                
            }
        }
    }

    open override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard !PianoNode.shared.isPlay else { return }
        for touch in touches {
            if selectedNodes[touch] != nil {
                if let noteNumber = selectedNodes[touch]?.noteNumber {
                    conductor.stopNote(note: noteNumber, channel: 0)
                }
                selectedNodes[touch]?.animateReleased()
                selectedNodes[touch] = nil
            }
        }
   }
}
