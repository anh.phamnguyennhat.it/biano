//
//  ViewController+CameraPermission.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 04/04/2021.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import Foundation
import UIKit
import ARKit

extension ViewController {
    func handleCameraPermission() {
        if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
            self.setup()
        } else {
            AVCaptureDevice.requestAccess(for: .video) { (granted: Bool) in
                if granted {
                    self.setup()
                } else {
                    self.settingCameraPermission()
                }
            }
        }
    }
    
    func setup() {
        self.prepareSongs()
        conductor.loadSamples(byIndex: 0)
        NetworkReachability.startListening()
        self.notifications()
        DispatchQueue.main.async {
            self.noConnectionLabel?.text = "noconnection".localized
        }
        self.sceneView.isUserInteractionEnabled = true
        self.registerGestureRecognizers()
        self.hideKeyboardWhenTappedAround()
        self.ARConfiguration()
        conductor.midi.addListener(self)
    }
    
    func settingCameraPermission() {
        DispatchQueue.main.async {
            let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String
            ?? Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String
            ?? ""
            let message = String(format: "permission".localized, appName)
            let alert = UIAlertController(title: "arfail".localized, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "settings".localized, style: .default, handler: { _ in self.openAppSettings() }))
            alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }

    }
    
    private func openAppSettings() {
        guard let url = URL(string: UIApplication.openSettingsURLString) else { return }
        DispatchQueue.main.async {
            UIApplication.shared.open(url)
        }
    }
}
