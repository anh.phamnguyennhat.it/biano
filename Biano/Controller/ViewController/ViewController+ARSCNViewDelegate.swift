//
//  ViewController+ARSCNViewDelegate.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 04/04/2021.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import Foundation
import ARKit

extension ViewController: ARSCNViewDelegate, ARSessionDelegate {
    
    // MARK: - ARSessionDelegate
    public func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        switch camera.trackingState {
        case .limited(.initializing):
            updateSessionInfoLabel(arStatus: .limited(.initializing))
        case .limited(.excessiveMotion):
            updateSessionInfoLabel(arStatus: .limited(.excessiveMotion))
        case .limited(.insufficientFeatures):
            updateSessionInfoLabel(arStatus: .limited(.insufficientFeatures))
        case .limited(.relocalizing):
            if #available(iOS 11.3, *) {
                updateSessionInfoLabel(arStatus: .limited(.relocalizing))
            } else {
                updateSessionInfoLabel(arStatus: .error)
            }
        case .notAvailable:
            updateSessionInfoLabel(arStatus: .error)
        case .normal:
            if isDetected {
                updateSessionInfoLabel(arStatus: .detected)
            } else {
                updateSessionInfoLabel(arStatus: .detecting)
            }
        default: break
        }
    }
    
    public func sessionWasInterrupted(_ session: ARSession) {
        updateSessionInfoLabel(arStatus: .error)
    }
    
    public func sessionInterruptionEnded(_ session: ARSession) {
        updateSessionInfoLabel(arStatus: .error)
    }
    
    public func session(_ session: ARSession, didFailWithError error: Error) {
        if error.localizedDescription.contains("Camera access not authorize") {
            settingCameraPermission()
        } else {
            updateSessionInfoLabel(arStatus: .error)
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard anchor is ARPlaneAnchor else { return }
        DispatchQueue.main.async {
            self.isDetected = true
            self.updateSessionInfoLabel(arStatus: .detected)
        }
        
    }
    
    public func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        guard anchor is ARPlaneAnchor else { return }
        isDetected = false
        if !PianoNode.shared.isPlaced {
            updateSessionInfoLabel(arStatus: .detecting)
        }
    }
}

extension ViewController {
    func updateSessionInfoLabel(arStatus: ARStatus) {
        var message: String
        self.lastARStatus = arStatus
        switch arStatus {
        case .detecting:
            message = "detecting".localized
        case .detected:
            setDetected()
            message = !PianoNode.shared.isPlaced ? "tapscreen".localized : ""
        case .error:
            message = "trackingunavailable".localized
        case .limited(.excessiveMotion):
            message = "slow".localized
        case .limited(.insufficientFeatures):
            message = "insufficient".localized
        case .limited(.initializing):
            message = !PianoNode.shared.isPlaced ? "initialize".localized : ""
        case .placed:
            message = "placed".localized
        default:
            message = ""
        }
        setMessage(message)
    }
    
    internal func setDetected() {
        DispatchQueue.main.async {
            self.moveImage?.isHidden = true
            self.sceneView.isUserInteractionEnabled = true
        }
    }
    
    private func setMessage(_ message: String) {
        DispatchQueue.main.async {
            self.messageView?.isHidden = message.isEmpty
            self.messageLabel?.text = message
        }
    }
}
