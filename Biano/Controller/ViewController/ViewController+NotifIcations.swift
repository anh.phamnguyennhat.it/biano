//
//  ViewController+Notifications.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 04/04/2021.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import Foundation
import UIKit

extension ViewController {
    internal func notifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(networkReachabilityChanged), name: NetworkReachability.networkReachabilityChangedNotification, object: nil)

    }
    
    @objc func applicationDidEnterBackground() {
        self.resetAR()
    }

    @objc func networkReachabilityChanged() {
        isCheckedNetwork = true
        guard self.realtimeSongs.count == 0 else {
            setNoConnectionView(hidden: true)
            return
        }
        guard NetworkReachability.isReachable else {
            setNoConnectionView(hidden: false)
            return
        }
    
        setNoConnectionView(hidden: true)
        self.prepareSongs()
        
    }
    
    func setNoConnectionView(hidden: Bool) {
        DispatchQueue.main.async {
            self.noConnectionView?.isHidden = hidden
        }
    }
}
