//
//  ViewController.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 2/10/20.
//  Copyright © 2020 jibistudio. All rights reserved.
//

import UIKit
import ARKit
import AudioKit
import Localize
import GoogleMobileAds

class ViewController: UIViewController {
    
    @IBOutlet internal weak var sceneView: ARSCNView!
    @IBOutlet internal weak var settingView: UIVisualEffectView!
    @IBOutlet internal weak var songsContainer: UIView!
    @IBOutlet internal weak var playSongButton: UIButton!
    @IBOutlet internal weak var resetARButton: UIButton!
    @IBOutlet internal weak var songsSearchBar: UISearchBar!
    @IBOutlet internal weak var songTableView: UITableView!
    @IBOutlet internal weak var updateSongsButton: UIButton!
    @IBOutlet internal weak var closeTableButton: UIButton!
    
    @IBOutlet internal weak var selectSongButton: UIButton!
    @IBOutlet internal weak var moveImage: UIImageView!
    @IBOutlet internal weak var noConnectionView: UIVisualEffectView!
    @IBOutlet internal weak var messageView: UIVisualEffectView!
    @IBOutlet internal weak var noConnectionLabel: UILabel!
    @IBOutlet internal weak var messageLabel: UILabel!
    
    @IBOutlet internal weak var bannerView: UIView!
    internal var adView: GADBannerView = {
        let view = GADBannerView(adSize: kGADAdSizeBanner)
        let id = "ca-app-pub-5218107787515927/9443102295"
        
        view.adUnitID = id 
        return view
    }()
    @IBOutlet internal weak var playButtonBottomAnchorConstraint: NSLayoutConstraint!
    
    var pianoStartScale = Float(0)
    var lastDragResult: ARHitTestResult?
    var configuration = ARWorldTrackingConfiguration()
    var songIndex = 0
    var isDetected = false
    var isCheckedNetwork = false

    let conductor = Conductor.shared
    var realtimeSongs: [Song] = []
    var filteredSongs: [Song] = []
    let songAPI = SongAPI()
    var lastARStatus: ARStatus?
    override var prefersStatusBarHidden: Bool {
        return true
    }
    var selectedNodes:[UITouch:PianoKeyNode] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        handleCameraPermission()
        songTableView.delegate = self
        songTableView.dataSource = self
        songsSearchBar.delegate = self
        playSongButton.setTitle("play".localized, for: .normal)
        
        addBannerView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !isCheckedNetwork {
          networkReachabilityChanged()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
      super.viewWillDisappear(animated)
      
      // Pause the view's session
      sceneView.session.pause()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    
    @IBAction func playSong(_ sender: Any) {
        if  conductor.midiFile == nil {
            playSongButton.isUserInteractionEnabled = false
            _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(playMidi), userInfo: nil, repeats: false)
        } else if let midiFile = conductor.midiFile, midiFile.isPlaying {
            handleARSceneWhenStopSong()
            hiddenSettingView()
        }
    }
    
    @objc func playMidi(){
        guard realtimeSongs.count > 0, songIndex < filteredSongs.count
        else {
            return
        }
        self.prepareMidiFile(song: filteredSongs[songIndex], completion: { [weak self] (url)  in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async {
                self.playSongButton.isUserInteractionEnabled = true
            }
            guard let url = url else {
                self.showAlert(alertText: "somethingwrong".localized, alertMessage: "\("cannotplay".localized) \("tryanother".localized)")
                return
            }
            if self.conductor.playMidiFile(url: url) {
                DispatchQueue.main.async {
                    self.playSongButton.setTitle("stop".localized, for: .normal)
                    self.resetARButton.isHidden = true
                    PianoNode.shared.isPlay = true
                    self.updateSessionInfoLabel(arStatus: .played)
                    self.hiddenSettingView()
                }
            } else {
                self.showAlert(alertText: "somethingwrong".localized, alertMessage: "\("cannotplay".localized) \("tryagain".localized)")
            }
        })
    }
    
    @objc func resetKeyboard(){
        PianoNode.shared.resetKeyboard()
    }
    @IBAction func openSongsList(_ sender: Any) {
        songsContainer.isHidden = !songsContainer.isHidden
        playSongButton.isHidden = !playSongButton.isHidden
        resetARButton.isHidden = !resetARButton.isHidden
        updateSessionInfoLabel(arStatus: .played)
    }
        
    @IBAction func updateSongsList(_ sender: Any) {
        showConfirmAlert(message:  "update".localized) {
            self.prepareSongs()
        }
    }
    
    @IBAction func resetARScene(_ sender: Any) {
        showConfirmAlert(message: "reset".localized) {
            self.resetAR()
        }
    }
    
}

extension ViewController {
    internal func ARConfiguration(){
        self.configuration.planeDetection = .horizontal
        self.configuration.environmentTexturing = .automatic
        self.sceneView.automaticallyUpdatesLighting = true
        self.sceneView.autoenablesDefaultLighting = true
        self.sceneView.debugOptions = [.showFeaturePoints]
        self.sceneView.delegate = self
        self.sceneView.session.run(configuration)
    }
    
    internal func hiddenSettingView(){
        let isPlay = PianoNode.shared.isPlay
        let isPlaced = PianoNode.shared.isPlaced
        let haveSongs = self.realtimeSongs.count != 0
        self.settingView.isHidden = !(haveSongs && isPlaced) || isPlay
    }
    
    internal func resetAR() {
        self.sceneView.scene.rootNode.enumerateChildNodes { (child, _) in
            if child is PianoNode {
                child.removeFromParentNode()
            }
        }
        
        PianoNode.shared.pianoKeyNodes.forEach { (pianoKeyNode) in
            pianoKeyNode.resetKey()
            pianoKeyNode.removeEffects()
        }
        
        self.handleARSceneWhenStopSong()
        isDetected = false
        self.hiddenSettingView()
        PianoNode.shared.isPlaced = false
        self.playSongButton.isHidden = true
        self.resetARButton.isHidden = true
        updateSessionInfoLabel(arStatus: .detecting)
        self.sceneView.debugOptions = [.showFeaturePoints]
        self.sceneView.session.run(configuration, options: [.removeExistingAnchors])
    }
    
    internal func handleARSceneWhenStopSong() {
        AppStoreReviewManager.requestReviewIfAppropriate()
        playSongButton.setTitle("play".localized, for: .normal)
        PianoNode.shared.isPlay = false
        resetARButton.isHidden = false
        conductor.stopMidiFile()
        _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(resetKeyboard), userInfo: nil, repeats: false)
    }
    
    internal func addBannerView() {
        playButtonBottomAnchorConstraint.constant = 10
        bannerView.isHidden = false
        bannerView.addSubview(adView)
        adView.rootViewController = self
        adView.leftAnchor.constraint(equalTo: bannerView.leftAnchor).isActive = true
        adView.rightAnchor.constraint(equalTo: bannerView.rightAnchor).isActive = true
        adView.topAnchor.constraint(equalTo: bannerView.topAnchor).isActive = true
        adView.bottomAnchor.constraint(equalTo: bannerView.bottomAnchor).isActive = true
        adView.load(GADRequest())
    }
}
