//
//  ARStatus.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 03/04/2021.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import Foundation

public enum ARStatus {
    case detecting
    case detected
    case placed
    case played
    case error
    case limited(Reason)
    
    public enum Reason {
        
        /** Tracking is limited due to initialization in progress. */
        case initializing
        
        /** Tracking is limited due to a excessive motion of the camera. */
        case excessiveMotion
        
        /** Tracking is limited due to a lack of features visible to the camera. */
        case insufficientFeatures
        
        /** Tracking is limited due to a relocalization in progress. */
        @available(iOS 11.3, *)
        case relocalizing
    }
}

