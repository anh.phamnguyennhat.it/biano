//
//  SongTableViewCell.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 07/04/2021.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import UIKit

class SongTableViewCell: UITableViewCell {

    
    @IBOutlet private weak var titleName: UILabel!
    @IBOutlet private weak var titleSinger: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func set(song: Song) {
        self.titleName.text = song.name
        self.titleSinger.text = song.singer
    }
}
