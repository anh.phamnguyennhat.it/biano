// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.3 (swiftlang-1200.0.29.2 clang-1200.0.30.1)
// swift-module-flags: -target arm64-apple-tvos10.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name AudioKitUI
import AudioKit
@_exported import AudioKitUI
import Swift
@objc @_inheritsConvenienceInitializers @IBDesignable open class AKOutputWaveformPlot : AudioKitUI.AKNodeOutputPlot {
  public static func createView(width: CoreGraphics.CGFloat = 440, height: CoreGraphics.CGFloat = 200.0) -> AudioKitUI.AKView
  @objc deinit
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc override public init(_ input: AudioKit.AKNode? = super, frame: CoreGraphics.CGRect = super, bufferSize: Swift.Int = super)
}
@objc @IBDesignable open class AKNodeOutputPlot : AudioKitUI.EZAudioPlot {
  public var isConnected: Swift.Bool
  public var isNotConnected: Swift.Bool {
    get
  }
  @objc open func pause()
  @objc open func resume()
  @objc open var node: AudioKit.AKNode? {
    @objc get
    @objc set
  }
  @objc deinit
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc public init(_ input: AudioKit.AKNode? = AKManager.output, frame: CoreGraphics.CGRect = CGRect.zero, bufferSize: Swift.Int = 1_024)
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
}
public typealias AKView = UIKit.UIView
public typealias AKColor = UIKit.UIColor
public enum AKTheme {
  case basic
  case midnight
  public static func == (a: AudioKitUI.AKTheme, b: AudioKitUI.AKTheme) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
}
@_hasMissingDesignatedInitializers public class AKStylist {
  public static let sharedInstance: AudioKitUI.AKStylist
  public var bgColor: AudioKitUI.AKColor {
    get
  }
  public var fontColor: AudioKitUI.AKColor {
    get
  }
  public var theme: AudioKitUI.AKTheme
  public var nextColor: AudioKitUI.AKColor {
    get
  }
  public var colorForTrueValue: AudioKitUI.AKColor {
    get
  }
  public var colorForFalseValue: AudioKitUI.AKColor {
    get
  }
  @objc deinit
}
@objc @_inheritsConvenienceInitializers open class AKWaveformLayer : QuartzCore.CAShapeLayer {
  public var allowActions: Swift.Bool
  public var isMirrored: Swift.Bool {
    get
    set
  }
  public var table: [Swift.Float]? {
    get
    set
  }
  public var isEmpty: Swift.Bool {
    get
  }
  public var gain: Swift.Float {
    get
    set
  }
  convenience public init(table: [Swift.Float], size: CoreGraphics.CGSize? = nil, fillColor: CoreGraphics.CGColor? = nil, strokeColor: CoreGraphics.CGColor? = nil, backgroundColor: CoreGraphics.CGColor? = nil, opacity: Swift.Float = 1, isMirrored: Swift.Bool = false)
  @objc override dynamic public func action(forKey event: Swift.String) -> QuartzCore.CAAction?
  public func updateLayer()
  public func updateLayer(with size: CoreGraphics.CGSize)
  public func dispose()
  @objc deinit
  @objc override dynamic public init()
  @objc override dynamic public init(layer: Any)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
}
@objc @IBDesignable open class AKRollingOutputPlot : AudioKitUI.AKNodeOutputPlot {
  @objc public init(frame: CoreGraphics.CGRect, bufferSize: Swift.Int = 1_024)
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  public static func createView(width: CoreGraphics.CGFloat = 440, height: CoreGraphics.CGFloat = 200.0) -> AudioKitUI.AKView
  @objc deinit
  @objc override public init(_ input: AudioKit.AKNode? = super, frame: CoreGraphics.CGRect = super, bufferSize: Swift.Int = super)
}
@objc @IBDesignable open class AKNodeFFTPlot : AudioKitUI.EZAudioPlot, AudioKit.EZAudioFFTDelegate {
  public var isConnected: Swift.Bool
  public var isNotConnected: Swift.Bool {
    get
  }
  @objc open func pause()
  @objc open func resume()
  @objc open var node: AudioKit.AKNode? {
    @objc get
    @objc set
  }
  @objc deinit
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc public init(_ input: AudioKit.AKNode?, frame: CoreGraphics.CGRect, bufferSize: Swift.Int = 1_024)
  @objc open func fft(_ fft: AudioKit.EZAudioFFT!, updatedWithFFTData fftData: Swift.UnsafeMutablePointer<Swift.Float>, bufferSize: Accelerate.vDSP_Length)
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
}
extension AudioKitUI.AKTheme : Swift.Equatable {}
extension AudioKitUI.AKTheme : Swift.Hashable {}
