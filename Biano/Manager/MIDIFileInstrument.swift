//
//  MIDIFileInstrument.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 4/12/20.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import Foundation
import AudioKit

// When having events from Midifile, method `play` and `stop` of `AKSampler` will be triggered, we need create our class inherit from `AKSampler` to add the animation when the note is pressed
class MIDIFileInstrument: AKSampler {
    
    override func play(noteNumber: MIDINoteNumber, velocity: MIDIVelocity, channel: MIDIChannel = 0) {
        super.play(noteNumber: noteNumber, velocity: velocity, channel: channel)
        if channel == 0 {
            PianoNode.shared.pianoKeyNodesDict[noteNumber]?.animatePressed(color: .blue)
        } else if channel == 1{
            PianoNode.shared.pianoKeyNodesDict[noteNumber]?.animatePressed(color: .systemPink)
        } else if channel == 2 {
            PianoNode.shared.pianoKeyNodesDict[noteNumber]?.animatePressed(color: .purple)
        }
    }
    
    override func stop(noteNumber: MIDINoteNumber) {
        super.stop(noteNumber: noteNumber)
        PianoNode.shared.pianoKeyNodesDict[noteNumber]?.animateReleased()
    }
}
