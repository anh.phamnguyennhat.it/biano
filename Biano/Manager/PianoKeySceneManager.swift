//
//  PianoKeySceneManager.swift
//  ARPiano
//
//  Created by Pham Nguyen Nhat Anh on 2/11/20.
//  Copyright © 2020 jibistudio. All rights reserved.
//

import Foundation
import ARKit

class PianoKeySceneManager {
    
    public static let sharedInstance = PianoKeySceneManager()
    let pianoKeyScene = SCNScene(named: "arts.scnassets/PianoKeys.scn")!
    private let whitePianoKey: SCNNode
    private let blackPianoKey: SCNNode
    private let defaultScale = SCNVector3(0.01, 0.01, 0.01)
    
    private init(){
        whitePianoKey = self.pianoKeyScene.rootNode.childNode(withName: "WhiteKey", recursively: true)!
        blackPianoKey = self.pianoKeyScene.rootNode.childNode(withName: "BlackKey", recursively: true)!
    }
    
    func getWhitePianoKey(name: String) -> SCNNode {
        let pianoKey = whitePianoKey.clone()
        pianoKey.geometry = whitePianoKey.geometry!.copy() as? SCNGeometry
        pianoKey.name = name
        pianoKey.scale = defaultScale
        let materialWhite = SCNMaterial()
        materialWhite.diffuse.contents = UIColor(white: 0.95, alpha: 1)
        materialWhite.specular.contents = UIColor.white
        materialWhite.emission.contents = UIColor.black
        
        pianoKey.geometry!.firstMaterial = materialWhite
        return pianoKey
    }
    
    func getBlackPianoKey(name: String) -> SCNNode {
        let pianoKey = blackPianoKey.clone()
        pianoKey.geometry = blackPianoKey.geometry!.copy() as? SCNGeometry
        pianoKey.name = name
        pianoKey.scale = defaultScale
        pianoKey.position = SCNVector3(0, 0, -0.0033)
        let materialBlack = SCNMaterial()
        materialBlack.diffuse.contents = UIColor(white: 0.1, alpha: 1)
        materialBlack.specular.contents = UIColor.white
        materialBlack.emission.contents = UIColor.black
        
        pianoKey.geometry!.firstMaterial = materialBlack
        
        return pianoKey
    }
    
}
