//
//  Conductor.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 3/12/20.
//  Copyright © 2020 jibistudio. All rights reserved.


import AudioKit
//takes in a number without a label which is the MIDINoteNumber or UInt8; takes in an argument labled semitones which is an Int; Returns a new MIDINoteNumber or UInt8
//create a constant with the value of an Int called nn which will be the note number. We use the Int function to convert the note from a UInt8 to just a regular Int
//return the new offset note, hence the formula semitones + original note, and notice that it is converted into a MIDINoteNumber Type
func offsetNote(_ note: MIDINoteNumber, semitones: Int) -> MIDINoteNumber {
    let nn = Int(note)
    return (MIDINoteNumber)(semitones + nn)
}

class Conductor {
    
    //Class methods can now be accessed using Conductor.shared.methodName()
    static let shared = Conductor()
    //Create a midi object referring to the class AKMIDI()
    let midi = AKMIDI()
    //Create a new sampler object of type AKAppleSampler
    var sampler: AKSampler
    var midiFile: AKAppleSequencer?
    var midiFileConnector: AKMIDINode!
    //Demonstrate what a mod wheel does, maybe go to Couch or the High School, or just film the pedal at home, or the OP-1
    var pitchBendUpSemitones = 2
    var pitchBendDownSemitones = 2

    //Assuming that this is the default offset, where the synth will just play the note the user is playing without any offset
    var synthSemitoneOffset = 0

    init() {
        midi.createVirtualPorts()
        midi.openInput(name: "Session 1")
        midi.openOutput()
        AKAudioFile.cleanTempDirectory()
        AKSettings.bufferLength = .medium
        AKSettings.enableLogging = true
        sampler = MIDIFileInstrument(masterVolume: 1.0)
        // Set up the AKSampler
        setupSampler()
    }
    
    public func playMidiFile(url: URL) -> Bool {
        midiFile = AKAppleSequencer(fromURL: url)
        //Sampler Object - Allows us to play a soundfont as an instrument
        guard  let midiFile = midiFile, midiFile.trackCount >= 1 else {
            self.midiFile = nil
            return false
        }
        var midiNoteDataTrackZero = midiFile.tracks[0].getMIDINoteData()
        for index in 0..<midiNoteDataTrackZero.count {
            midiNoteDataTrackZero[index].channel = 0
        }
        midiFile.tracks[0].replaceMIDINoteData(with: midiNoteDataTrackZero)
        
        if midiFile.trackCount >= 2 {
            var midiNoteDataTrackOne = midiFile.tracks[1].getMIDINoteData()
            for index in 0..<midiNoteDataTrackOne.count {
                midiNoteDataTrackOne[index].channel = 1
            }
            midiFile.tracks[1].replaceMIDINoteData(with: midiNoteDataTrackOne)
        }
        
        
    
        midiFile.setGlobalMIDIOutput(midiFileConnector.midiIn)
        //Set the volume property of our new sampler object

        midiFile.play()
        return true
    }
    
    public func stopMidiFile(){
        midiFile?.stop()
        midiFile = nil
    }

    func addMIDIListener(_ listener: AKMIDIListener) {
        midi.addListener(listener)
    }

    //Method to get the midi input names and returns them in an array/list of Strings
    func getMIDIInputNames() -> [String] {
        return midi.inputNames
    }

    //Method to open a specific MIDI input port by name
    func openMIDIInput(byName: String) {
        //Close all MIDI input ports which are already open
        midi.closeAllInputs()
        
        //Same as the open input function above and takes in the name from this method
        midi.openInput(name: byName)
    }

    //Method to open a specific MIDI input port by index - the number in the array
    func openMIDIInput(byIndex: Int) {
        
        //Close all MIDI input ports which are already open
        midi.closeAllInputs()
        
        //Notice how the name argument is the same, but uses indices to get the name
        midi.openInput(name: midi.inputNames[byIndex])
    }

    //Method to load a sample and takes in the index (number of samples to load). we don't load more than 4 at once, and we can't load negative samples. That doesn't make sense.
    func loadSamples(byIndex: Int) {
        
        //exit the function if the sample index is negative or greater than 3
        if byIndex < 0 || byIndex > 3 { return }

        //Access The Process Info Instance via Pointee <NSProcessInfo: Address Number>
        let info = ProcessInfo.processInfo
        
        //Access the systemUptime property which inherits from the NSProcessInfo
        //Notice we keep track of this as a constant
        let begin = info.systemUptime

        let sfzFiles = ["G800-A112-Piano1d-2-m.sfz"]
        
        sampler.loadSFZ(path: Bundle.main.resourcePath! + "/", fileName: sfzFiles[byIndex])
        //load the actual .wav samples
        sampler.loadSfzWithEmbeddedSpacesInSampleNames(folderPath: Bundle.main.resourcePath! + "/",
                                                       sfzFileName: sfzFiles[byIndex])
        print(info.systemUptime, "hi")
        let elapsedTime = info.systemUptime - begin
        AKLog("Time to load samples \(elapsedTime) seconds")
    }

    func playNote(note: MIDINoteNumber, velocity: MIDIVelocity, channel: MIDIChannel) {
        AKLog("playNote \(note) \(velocity)")
        sampler.play(noteNumber: offsetNote(note, semitones: synthSemitoneOffset), velocity: velocity)
    }

    func stopNote(note: MIDINoteNumber, channel: MIDIChannel) {
        AKLog("stopNote \(note)")
        sampler.stop(noteNumber: offsetNote(note, semitones: synthSemitoneOffset))
    }

    func allNotesOff() {
        sampler.stopAllVoices()
    }

    func afterTouch(_ pressure: MIDIByte) {
    }

    func controller(_ controller: MIDIByte, value: MIDIByte) {
        switch controller {
        case AKMIDIControl.modulationWheel.rawValue:
            if sampler.filterEnable {
                sampler.filterCutoff = 1 + 19 * Double(value) / 127.0
            } else {
                sampler.vibratoDepth = 0.5 * Double(value) / 127.0
            }

        case AKMIDIControl.damperOnOff.rawValue:
            sampler.sustainPedal(pedalDown: value != 0)

        default:
            break
        }
    }

    //A MIDIWord means that it is an Unsigned 16 bit number ranging from 0 to 255 Unsigned means positive
    func pitchBend(_ pitchWheelValue: MIDIWord) {
        let pwValue = Double(pitchWheelValue)
        let scale = (pwValue - 8_192.0) / 8_192.0
        if scale >= 0.0 {
            sampler.pitchBend = scale * self.pitchBendUpSemitones
        } else {
            sampler.pitchBend = scale * self.pitchBendDownSemitones
        }
    }

    deinit {
        do {
            try AKManager.stop()
        } catch {
            AKLog("AudioKit did not start")
        }
    }
}


extension Conductor {
    
    private func setupSampler() {
        sampler.attackDuration = 0.01
        sampler.decayDuration = 0.1
        sampler.sustainLevel = 0.8
        sampler.releaseDuration = 0.5

        sampler.filterEnable = true
        sampler.filterCutoff = 20.0
        sampler.filterAttackDuration = 1.0
        sampler.filterDecayDuration = 1.0
        sampler.filterSustainLevel = 0.5
        sampler.filterReleaseDuration = 10.0
        
        AKManager.output = sampler
        do {
            try AKManager.start()
        } catch {
            AKLog("AudioKit did not start")
        }
        midiFileConnector = AKMIDINode(node: sampler)
    }

}



