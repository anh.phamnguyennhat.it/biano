//
//  PianoNode.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 2/10/20.
//  Copyright © 2020 jibistudio. All rights reserved.
//

import Foundation
import SceneKit
class PianoNode: SCNNode {
    static var shared: PianoNode = PianoNode()
    var pianoKeyNodes : [PianoKeyNode] = []
    var pianoKeyNodesDict: [UInt8: PianoKeyNode] = [:]
    var whiteKeyNodes: [PianoKeyNode] = []
    var blackKeyNodes: [PianoKeyNode] = []
    let midiNumberConvert = 20
    var isPlaced = false
    var isPlay = false
    
    // keyboard map
    private let pianoTypes: [PianoKeyType] = [
        PianoKeyType.white, PianoKeyType.black, PianoKeyType.white,
        PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white,
        PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white,
        PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white,
        PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white,
        PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white,
        PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white,
        PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white, PianoKeyType.black, PianoKeyType.white,
        PianoKeyType.white
    ]
    
    override private init() {
        super.init()
        self.setupKeyBoard()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupKeyBoard(){
        for i in 0..<pianoTypes.count {
            let numberNote = i+midiNumberConvert+1
            let pianoKeyNode = PianoKeyNode(pianoType: pianoTypes[i], numberNote: numberNote)
            pianoKeyNode.opacity = 1.0
            if pianoTypes[i] == .white {
                whiteKeyNodes.append(pianoKeyNode)
            } else {
                blackKeyNodes.append(pianoKeyNode)
            }
            let convertNumberNote = UInt8(numberNote)
            pianoKeyNode.noteNumber = convertNumberNote
            pianoKeyNodesDict[convertNumberNote] = pianoKeyNode
            pianoKeyNodes.append(pianoKeyNode)
        }
        let whiteKeyNodesCount = whiteKeyNodes.count
        var whiteKeyIndex = 0
        for i in 0..<pianoKeyNodes.count {
            if pianoKeyNodes[i].type == .white {
                pianoKeyNodes[i].position = SCNVector3((PianoKeyNode.lengthKeyNode+PianoKeyNode.spacing)*CGFloat(whiteKeyIndex-whiteKeyNodesCount/2), 0, 0)
                whiteKeyIndex += 1
            } else {
                let xPositionWhiteKey = (PianoKeyNode.lengthKeyNode+PianoKeyNode.spacing)*CGFloat(whiteKeyIndex-whiteKeyNodesCount/2)
                let xPositionBlackKey = xPositionWhiteKey-(PianoKeyNode.lengthKeyNode+PianoKeyNode.spacing)/2
                pianoKeyNodes[i].position = SCNVector3(xPositionBlackKey, 0.005, 0)
            }
            self.addChildNode(pianoKeyNodes[i])
        }
        self.scale = SCNVector3(1, 1, 1)
    }

    public func resetKeyboard() {
        pianoKeyNodes.forEach { (pianoKeyNode) in
            pianoKeyNode.resetKey()
        }
    }
    
    public func zoom(scale: Float, force: Bool = false) {
        PianoNode.shared.scale = SCNVector3(scale, scale, scale)
    }
}
