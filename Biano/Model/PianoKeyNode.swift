//
//  PianoKeyNode.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 2/10/20.
//  Copyright © 2020 jibistudio. All rights reserved.
//

import UIKit
import SceneKit

enum PianoKeyType {
    case white
    case black
}

class PianoKeyNode : SCNNode {
    static let lengthKeyNode: CGFloat =  0.007
    static let spacing: CGFloat = 0.0001
    var type: PianoKeyType
    var modelNode: SCNNode?
    var noteNumber: UInt8?
    var initialPositionY: Float {
        return type == .white ? 0 : 0.005
    }
    
    init(pianoType: PianoKeyType, numberNote: Int) {
        self.type = pianoType
        super.init()
        self.name = String(describing: numberNote)
        self.setupKeyNode()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func animatePressed(color: UIColor){
        guard let modelNode = self.modelNode else { return }
        let material = modelNode.geometry!.firstMaterial!
        material.emission.contents = color
        self.position.y = self.initialPositionY - 0.002
        
        shotBubble(modelNode: modelNode, color: color, pianoKeyType: self.type)
        addParticle(modelNode: modelNode, color: color)
    }
    
    func animateReleased(){
        resetKey()
    }
    
    func resetKey() {
        self.position.y = self.initialPositionY
        self.modelNode?.geometry?.firstMaterial?.emission.contents = UIColor.black
        self.modelNode?.removeAllParticleSystems()
    }
    
    func removeEffects() {
        modelNode?.enumerateChildNodes { (child, _) in
            if child is BubbleNode || child.name == "particle" {
                child.removeAllParticleSystems()
                child.removeFromParentNode()
            }
        }
    }
    
    private func setupKeyNode () {
        guard let name = name else { return }
        if type == .white {
            let keyNode = PianoKeySceneManager.sharedInstance.getWhitePianoKey(name: name)
            keyNode.name = "model"
            self.addChildNode(keyNode)
        } else {
            let keyNode = PianoKeySceneManager.sharedInstance.getBlackPianoKey(name: name)
            keyNode.name = "model"
            self.addChildNode(keyNode)
        }
        self.modelNode = self.childNode(withName: "model", recursively: true)!
    }
    
    private func shotBubble(modelNode: SCNNode, color: UIColor, pianoKeyType: PianoKeyType) {
        let bubbleNode = BubbleNode(pianoKeyType: pianoKeyType)
        modelNode.addChildNode(bubbleNode)
        bubbleNode.geometry?.firstMaterial?.emission.contents = color
        bubbleNode.shot()
    }
    
    private func addParticle(modelNode: SCNNode, color: UIColor) {
        let particle = SCNParticleSystem(named: "fire.scnp", inDirectory: "arts.scnassets")
        let particleNode = SCNNode()
        particleNode.addParticleSystem(particle!)
        particleNode.name = "particle"
        let scale = PianoNode.shared.scale
        particleNode.scale = SCNVector3(scale.x/2,scale.y/2, scale.z/2)
        particle?.particleColor = color
        particle?.emitterShape = self.modelNode?.geometry
        modelNode.addChildNode(particleNode)
    }
    
    
}
