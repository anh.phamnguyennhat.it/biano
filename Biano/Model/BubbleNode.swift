//
//  BubbleNode.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 4/12/20.
//  Copyright © 2020 jibistudio. All rights reserved.
//

import Foundation
import SceneKit

class BubbleNode: SCNNode {
    init(pianoKeyType: PianoKeyType) {
        super.init()
        self.geometry = getBubbleGeometry(pianoKeyType: pianoKeyType)
        self.position.z = pianoKeyType == .white ? -2.225 : -1.765 // position for bubble shot from white keynode and black keynode
    }

    private func getBubbleGeometry(pianoKeyType: PianoKeyType) -> SCNSphere {
        let radius : CGFloat = pianoKeyType == .white ? 0.327 : 0.161 // size for bubble shot from white keynode and black keynode
        let bubble = SCNSphere(radius: radius)
        bubble.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "icon")
        bubble.firstMaterial?.transparency = 0.1
        bubble.firstMaterial?.writesToDepthBuffer = false
        bubble.firstMaterial?.blendMode = .screen
        bubble.firstMaterial?.reflective.contents = #imageLiteral(resourceName: "bubble")
        return bubble
    }
    
    func shot(){
        let speed: TimeInterval = 5
        let toPosition = SCNVector3Make(0, 0, self.position.z - 50)
        let move = SCNAction.move(by: toPosition, duration: speed)
        self.runAction(move) {
            self.removeFromParentNode()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
