//
//  Song.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 3/20/20.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import Foundation

class Song: Codable {
    let name: String
    let singer: String?
    let filename: String
    let urlString: String
    
    init(name: String, filename: String, urlString: String, singer: String? = nil){
        self.name = name
        self.filename = filename
        self.urlString = urlString
        self.singer = singer
    }
}

extension Song {
    
    var fileNameWithExtension: String {
        return filename.contains(".mid") ? filename : filename + ".mid"
    }
    
    var url: URL? {
        return checkFileExists()
    }
    
    func download(completion: @escaping ((URL?) -> Void)) {
        guard let alarm = URL(string: self.urlString) else {
            completion(nil)
            return
        }
        do {
            try alarm.download(to: .documentDirectory, using: self.fileNameWithExtension) { url, error in
                completion(url)
            }
        } catch {
            print(error)
            completion(nil)
        }
    }
    
    private func checkFileExists() -> URL? {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent(fileNameWithExtension) {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            return fileManager.fileExists(atPath: filePath) ? pathComponent : nil
        } else {
            return nil
        }
    }
}

//let songs: [Song] = [
//    Song(name: "10 years", filename: "nam"),
//    Song(name: "Sweet night", filename: "Sweet_night2"),
//    Song(name: "Kiminonawa", filename: "kimi"),
//    Song(name: "The name of life", filename: "summer"),
//    Song(name: "Có chàng trai viết lên cây", filename: "cay"),
//    Song(name: "Love Scenario", filename: "love"),
//    Song(name: "More than love", filename: "HCY"),
//    Song(name: "Bad guy", filename: "bad"),
//    Song(name: "Someone like you", filename: "some"),
//    Song(name: "Don't know what to do", filename: "BL"),
//    Song(name: "Coffin dance", filename: "c"),
//    Song(name: "Goblin", filename: "S")
//]
