//
//  NetworkReachability.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 03/04/2021.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import Foundation
import Alamofire

struct NetworkReachability {
    private static let networkReachability = NetworkReachability()
    private init() { }
    private let manager = NetworkReachabilityManager()
    
    static func startListening() {
        networkReachability.manager?.startListening(onUpdatePerforming: { (status) in
            NotificationCenter.default.post(name: networkReachabilityChangedNotification, object: nil, userInfo: ["status": status])
        })
    }
    
    static var isReachable: Bool {
        return networkReachability.manager?.isReachable ?? false
    }
    
    static let networkReachabilityChangedNotification = NSNotification.Name("networkReachabilityChangedNotification")
}
