//
//  ProgressHUBManager.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 28/03/2021.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import Foundation
import UIKit

class ProgressHUBManager {
    
    static let shared = ProgressHUBManager()
    private var hud: HKProgressHUD?
    
    private init() {}
    
    func showIndicator(view: UIView) {
        DispatchQueue.main.async {
            self.hud = HKProgressHUD.show(addedToView: view, animated: true)
        }
    }
    
    func hideIndicator() {
        DispatchQueue.main.async {
            self.hud?.hide(animated: true)
        }
    }
    
}
