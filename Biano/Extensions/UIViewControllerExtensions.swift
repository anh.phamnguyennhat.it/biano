//
//  UIViewControllerExtensions.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 3/19/20.
//  Copyright © 2020 jibistudio. All rights reserved.
//

import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showAlert(alertText : String, alertMessage : String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: alertText, message: alertMessage, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "gotit".localized, style: UIAlertAction.Style.default, handler: nil))
            //Add more actions as you see fit
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showConfirmAlert(message: String, completion: @escaping () -> Void) {
        let updateAlert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)

        updateAlert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: { (action: UIAlertAction!) in
            completion()
        }))

        updateAlert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))

        present(updateAlert, animated: true, completion: nil)
    }
}
