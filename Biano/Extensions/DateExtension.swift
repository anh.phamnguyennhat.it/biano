//
//  DateExtension.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 03/04/2021.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import Foundation

extension Date {
    
    var isToday: Bool {
        return Calendar.current.isDateInToday(self)
    }
}
