//
//  IntExtensions.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 2/20/20.
//  Copyright © 2020 jibistudio. All rights reserved.
//

import Foundation

extension Int {
    var degressToRadians: Double { return Double(self) * .pi/180 }
}
