//
//  UserDefaultsExtension.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 31/03/2021.
//  Copyright © 2021 jibistudio. All rights reserved.
//

import Foundation

extension UserDefaults {
    private struct Keys {
        static let songs = "songs"
        static let localVersion = "localVersion"
        static let latestUpdate = "latestUpdate"
    }
        
    class var songs: [Song]? {
        get { return standard.songFor(key: Keys.songs) }
        set { standard.setSongs(songs: newValue, forKey: Keys.songs) }
    }
    class var localVersion: String? {
        get { return standard.string(forKey: Keys.localVersion) }
        set { standard.set(newValue, forKey: Keys.localVersion) }
    }
    
    class var latestUpdate: Date? {
        get { return standard.object(forKey: Keys.latestUpdate) as? Date }
        set { standard.set(newValue, forKey: Keys.latestUpdate) }
    }

    func songFor(key: String) -> [Song]? {
        if let songData = data(forKey: Keys.songs) {
            return try? JSONDecoder().decode([Song].self, from: songData)
        }
        return nil
    }

    func setSongs(songs: [Song]?, forKey key: String) {
        if let encoded = try? JSONEncoder().encode(songs){
            set(encoded, forKey: key)
            synchronize()
        }
    }
    
 
}
