//
//  SCNNodeExtensions.swift
//  Biano
//
//  Created by Pham Nguyen Nhat Anh on 4/15/20.
//  Copyright © 2020 jibistudio. All rights reserved.
//

import SceneKit

extension SCNNode {
    func distance(to B: SCNNode) -> Float {
        return Float(sqrt(pow((self.position.x - B.position.x), 2) + pow((self.position.y - B.position.y), 2) + pow((self.position.z - B.position.z), 2)))
    }
    
    func distanceXZ(to B: SCNNode) -> Float {
        return Float(sqrt(pow((self.position.x - B.position.x), 2) + pow((self.position.z - B.position.z), 2)))
    }
    
    func distanceZ(to B: SCNNode) -> Float {
        return Float(sqrt(pow((self.position.y - B.position.y), 2)))
    }
}
