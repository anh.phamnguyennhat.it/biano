# Biano

**An application simulates a piano in Augmented Reality environment and allows it play songs automatically by importing MIDI files.**

**Youtube channel:** https://www.youtube.com/c/BianoOfficial

# How to use Biano App 
Demo: https://www.youtube.com/watch?v=q6vqHJDE0FQ

1. Detect a plane to have a place for the piano
2. Tap on the screen to place the piano 
3. Move, rotate, resize to accommodate the space
4. Select the song 
5. Tap on the **Play** button 
6. Enjoy the show!
